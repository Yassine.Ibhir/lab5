 //@author Yassine Ibhir
package lab5.movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor(String sourceDir,String outDir) {
		super(sourceDir,outDir,true);
	}
	
	/** Overridden abstract method
	 * @param input An ArrayList<String> representing the input before processing
	 * @return Returns an ArrayList<String> representing the contents of the files in lowerCase
	 */
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (String line : input) {
			
			asLower.add(line.toLowerCase());
		}
		
		
		return asLower;
	}

}

 //@author Yassine Ibhir
package lab5.movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates(String srcDir,String outDir) {
		
		super(srcDir,outDir,false);
	}
	
	/** Overridden abstract method
	 * @param input An ArrayList<String> representing the input before processing
	 * @return Returns an ArrayList<String> representing the contents of the files with no duplicates
	 */
	
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList<String> noDuplicates = new ArrayList<String>();
		
		for(String line : input) {
			if(!noDuplicates.contains(line)) {
				noDuplicates.add(line);
			}
		}
		
		return noDuplicates;
	}
	

}
